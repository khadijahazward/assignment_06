#include <stdio.h>

//function to return fibonacci numbers
int fib(int number){
	if(number == 0){
		return 0;
	}else if (number == 1){
		return 1;
	}else{
		return fib(number - 1) + fib(number - 2);
	}
}

//prints all the fibonacci numbers upto a range.
int fibseq(int number){
	for(int i = 0; i <= number;i++){
	printf("%d\n",fib(i));
	}
}
int main(){
	int number;
	printf("Enter a range to check the fibonacci numbers\n");
	scanf("%d",&number);
	fibseq(number);

}
