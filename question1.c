#include <stdio.h>


//function to print the values in a row
int row (int num){
	for(int i = num; i >=1; i--)
	{
		printf("%d", i);
	}
}

//function to print the pattern 
int  pattern(int num){
	if(num==0){
		return 0;
	}else{
		pattern(num - 1);
		row(num);
		printf("\n");
	}
}
int main(){
	int num;
	printf("Enter a Number:\n");
	scanf("%d",&num);
	pattern(num);
	return 0;
}
